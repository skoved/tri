/*
Copyright © 2021 Sam Koved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package todo

import (
  "encoding/json"
  "io/ioutil"
  "strconv"
)

type Item struct {
  Text      string
  Priority  int
  position  int
  Done      bool
}

func SaveItems(filename string, items []Item) error {
  b, err := json.Marshal(items)
  if err != nil {
    return err
  }

  err = ioutil.WriteFile(filename, b, 0644)
  if err != nil {
    return err
  }

  return nil
}

func ReadItems(filename string) ([]Item, error) {
  b, err := ioutil.ReadFile(filename)
  if err != nil {
    return []Item{}, nil
  }

  var items []Item
  if err := json.Unmarshal(b, &items); err != nil {
    return []Item{}, err
  }

  for i, _ := range items {
    items[i].position = i+1
  }

  return items, nil
}

func (i *Item) SetPriority(pri int) {
  switch pri {
    case 1:
      i.Priority = 1
    case 3:
      i.Priority = 3
    default:
      i.Priority = 2
  }
}

func (i *Item) PrettyP() string {
  if i.Priority == 1 {
    return "(1)"
  }
  if i.Priority == 2 {
    return "(2)"
  }
  if i.Priority == 3 {
    return "(3)"
  }

  return " "
}

func (i *Item) PrettyDone() string {
  if i.Done {
    return "X"
  } else {
    return ""
  }
}

func (i *Item) Label() string {
  return strconv.Itoa(i.position) + "."
}

// ByPri implements sort.Interface for []Item based on 
// the Priority & position field
type ByPri []Item

func (s ByPri) Len() int { return len(s) }
func (s ByPri) Swap(i, j int) {s[i], s[j] = s[j], s[i] }
func (s ByPri) Less(i, j int) bool {
  if s[i].Done != s[j].Done {
    return s[i].Done
  }

  if s[i].Priority == s[j].Priority {
    return s[i].position < s[j].position
  }
  return s[i].Priority < s[j].Priority
}
